﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameOfLife
{
    class Program
    {
        static void Main(string[] args)
        {

            Random r = new Random();

            byte[,] grounds = new byte[500, 500];

            for (int x = 0; x < grounds.GetLength(0); x++)
                for (int y = 0; y < grounds.GetLength(1); y++)
                    grounds[x, y] = r.NextDouble() > 0.5 ? (byte)1 : (byte)0;

            Bitmap bmp = new Bitmap(500, 500);

            for (int x = 0; x < grounds.GetLength(0); x++)
                for (int y = 0; y < grounds.GetLength(1); y++)
                    bmp.SetPixel(x, y, grounds[x, y] == 1 ? Color.Black : Color.White);

            bmp.Save(@"C:\Users\Barend\Desktop\aa.png");

        }

        static byte[,] processMatrix(byte[,] matrix)
        {
            byte[,] newMatrix = new byte[matrix.GetLength(0), matrix.GetLength(1)];

            for (int x = 0; x < matrix.GetLength(0); x++)
                for (int y = 0; y < matrix.GetLength(1); y++)
                {

                    int n = getNumberOfNeighbours(x, y, matrix);

                    if (n < 2 && matrix[y, x] == 1)
                        newMatrix[x, y] = 0;

                    else if ((n == 2 || n == 3) && matrix[y, x] == 1)
                        newMatrix[x, y] = 1;

                    else if (n > 3 && matrix[y, x] == 1)
                        newMatrix[x, y] = 0;

                    else if (n == 3 && matrix[x, y] == 0)
                        newMatrix[x, y] = 1;
                }

            return newMatrix;
        }

        static int getNumberOfNeighbours(int x, int y, byte[,] matrix)
        {

            int n = 0;
            if (x != 0 && matrix[x - 1, y] == 1)
                n = n + 1;
            if (y != 0 && matrix[x, y - 1] == 1)
                n = n + 1;
            if (y + 1 < matrix.GetLength(1) && matrix[x, y + 1] == 1)
                n = n + 1;
            if (x + 1 < matrix.GetLength(0) && matrix[x + 1, y] == 1)
                n = n + 1;
            if (x + 1 < matrix.GetLength(0) && y + 1 < matrix.GetLength(1) && matrix[x + 1, y + 1] == 1)
                n = n + 1;
            if (x != 0 && y != 0 && matrix[x - 1, y - 1] == 1)
                n = n + 1;
            if (x + 1 < matrix.GetLength(0) && y != 0 && matrix[x + 1, y - 1] == 1)
                n = n + 1;
            if (y + 1 < matrix.GetLength(1) && x != 0 && matrix[x - 1, y + 1] == 1)
                n = n + 1;
            return n;
        }
    }
}
