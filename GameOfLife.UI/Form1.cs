﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows.Forms;

namespace GameOfLife.UI
{
    public partial class Form1 : Form
    {
        private byte[,] grounds = null;
        public Form1()
        {
            InitializeComponent();

            grounds = getGrounds();

            System.Timers.Timer timer = new System.Timers.Timer(500);
            timer.Elapsed += run;
            timer.Start();

        }

        private void run(Object source, ElapsedEventArgs e)
        {
            grounds = processMatrix(grounds);
            Bitmap bmp = matrixToBitmap(grounds);
            pictureBox1.Image = bmp;
        }

        private byte[,] getGrounds()
        {
            Random r = new Random();

            byte[,] grounds = new byte[500, 500];

            for (int x = 0; x < grounds.GetLength(0); x++)
                for (int y = 0; y < grounds.GetLength(1); y++)
                    grounds[x, y] = r.NextDouble() > 0.7 ? (byte)1 : (byte)0;

            return grounds;
        }

        private Bitmap matrixToBitmap(byte[,] matrix)
        {
            Bitmap bmp = new Bitmap(500, 500);

            for (int x = 0; x < matrix.GetLength(0); x++)
                for (int y = 0; y < matrix.GetLength(1); y++)
                    bmp.SetPixel(x, y, matrix[x, y] == 1 ? Color.Black : Color.White);

            return bmp;
        }

        private byte[,] processMatrix(byte[,] matrix)
        {
            byte[,] newMatrix = new byte[matrix.GetLength(0), matrix.GetLength(1)];

            for (int x = 0; x < matrix.GetLength(0); x++)
                for (int y = 0; y < matrix.GetLength(1); y++)
                {

                    int n = getNumberOfNeighbours(x, y, matrix);

                    if (n < 2 && matrix[y, x] == 1)
                        newMatrix[x, y] = 0;

                    else if ((n == 2 || n == 3) && matrix[y, x] == 1)
                        newMatrix[x, y] = 1;

                    else if (n > 3 && matrix[y, x] == 1)
                        newMatrix[x, y] = 0;

                    else if (n == 3 && matrix[x, y] == 0)
                        newMatrix[x, y] = 1;
                }

            return newMatrix;
        }

        private int getNumberOfNeighbours(int x, int y, byte[,] matrix)
        {

            int n = 0;
            if (x != 0 && matrix[x - 1, y] == 1)
                n = n + 1;
            if (y != 0 && matrix[x, y - 1] == 1)
                n = n + 1;
            if (y + 1 < matrix.GetLength(1) && matrix[x, y + 1] == 1)
                n = n + 1;
            if (x + 1 < matrix.GetLength(0) && matrix[x + 1, y] == 1)
                n = n + 1;
            if (x + 1 < matrix.GetLength(0) && y + 1 < matrix.GetLength(1) && matrix[x + 1, y + 1] == 1)
                n = n + 1;
            if (x != 0 && y != 0 && matrix[x - 1, y - 1] == 1)
                n = n + 1;
            if (x + 1 < matrix.GetLength(0) && y != 0 && matrix[x + 1, y - 1] == 1)
                n = n + 1;
            if (y + 1 < matrix.GetLength(1) && x != 0 && matrix[x - 1, y + 1] == 1)
                n = n + 1;
            return n;
        }
    }
}
